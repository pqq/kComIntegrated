
# ---------------------------------------------
# filename:	kCom.pm
# author:	Frode Klevstul (frode@klevstul.com)
# started:	23.10.2002
# version:	v20070528
# ---------------------------------------------



# --------------------------------------------------
# package setup
# --------------------------------------------------
package kCom;
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(
	kCom_getIniValue
	kCom_printTop
	kCom_printBottom
	kCom_tableStart
	kCom_tableStop
	kCom_table
	kCom_error
	kCom_printError
	kCom_timestamp
	kCom_log
	kCom_addComment
	kCom_getHostIP
	kCom_listComments
	kCom_printDate
	kCom_getLatest
	kCom_kFeeder_getLatestComments
	kCom_kIndexPlugin_news
	kCom_kIndexPlugin_contactInfo
	kCom_getMostPop
	kCom_kFeeder_getMostPop
	kCom_getAgeOfFileString
	kCom_getSortableNumberString
	kCom_jumpTo
	kCom_parseBBCode
	kCom_login
	kCom_notLoggedInMessage
	kCom_setCookie
	kCom_getCookie
	kCom_verifyCode
	kCom_generateVerificationCode
);

#	kCom_HTMLify
#	kCom_deHTMLify
#	kCom_sendEmail
#	kCom_makeURL



# -----------------------------------------------
# Global declarations
# -----------------------------------------------



# -----------------------------------------------
# sub rutines
# -----------------------------------------------



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Opens a file and returns a parameter value.
#			The file has to be on the format:
#			PARAMETER_NAME		PARAMETER_VALUE
#			(name and value has to be separated by tab)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_getIniValue{
	my $ini_file 			= $_[0];			# the name of the file that is going to be opened
	my $parameter			= $_[1];			# the name of the parameter to what we want it's value
	my $ignore_value		= $_[2];			# if this variable is equal to 1, then we ignore if no value is found
	my $stop_value			= $_[3];			# this variable tells us if we just want first entry in a file or all values
												# if no value (undef), returns all entries. if value == 1, return first entrie
	my $ignore_missingFile	= $_[4];			# if this variable is equal to 1 we'll ignore if the .ini file doesn't exsits

	my $line;
	my @value;									# NB: This procedure returns an array!

	if ($ini_file eq "kCom"){
		$ini_file	= "../kCom.ini";
	} elsif ($ini_file eq "kIndex"){
		$ini_file	= "../kIndex/kIndex.ini";
	} elsif ($ini_file eq "kIndexPlugin"){
		$ini_file	= "../kIndexPlugin/kIndexPlugin.ini";
	} elsif ($ini_file eq "kNews"){
		$ini_file	= "../kNews/kNews.ini";
	} elsif ($ini_file eq "kLogin"){
		$ini_file	= "../kLogin/kLogin.ini";
	} elsif ($ini_file =~ m/^kFeeder(.*)/){
		$ini_file	= "../kFeeder/kFeeder".$1.".ini";
	} elsif ($ini_file eq "kSearch"){
		$ini_file	= "../kSearch/kSearch.ini";
	}

    if($ignore_missingFile=undef){
	    open(FILE_INI, "<$ini_file") || &kCom_error("kCom_getIniValue","failed to open '$ini_file'");
	} else {
	    open(FILE_INI, "<$ini_file");
	    $ignore_value = 1;																			# if we ignore missing file there is no need for not ignoring missing value, so override that
	}
	LINE: while ($line = <FILE_INI>){
		if ($line =~ m/^#/){
			next LINE;
		} elsif ($line =~ m/^$parameter(\t)+(.*)$/){
			push(@value,$2);
			if ($stop_value == 1){
				last LINE;
			}
		}
	}
	close (FILE_INI);

	if ($value[0] eq "" and $ignore_value < 1){
		&kCom_error("kCom_getIniValue","failed to find a value for parameter '$parameter' in '$ini_file'");
	}

	if ($value[0] eq "NULL"){
		$value[0] = "";
	}

	return @value;
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Writes out an error and dies
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_error{
	my $module		= $_[0];
	my $error_msg	= $_[1];
	die "Error in module '$module': $error_msg\n";
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Prints out an error in HTML format
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_printError{
	my $module			= $_[0];
	my $error_msg		= $_[1];

	print qq(<div class="errorMessage">Error in module '$module': $error_msg</div><br><br>\n);
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Prints the start of every page
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_printTop{
	my $active_module	= $_[0];
	my @title 			= &kCom_getIniValue("kCom","HTML_PAGETITLE");
	my @css				= &kCom_getIniValue("kCom","HTML_CASCADINGSTYLESHEET");
	my @favicon			= &kCom_getIniValue("kCom","HTML_FAVICON");
	my @pagewidth		= &kCom_getIniValue("kCom","HTML_PAGEWIDTH");
	my @toplinks		= &kCom_getIniValue("kCom","HTML_TOPLINKS");
	my @url				= &kCom_getIniValue("kCom","URL_KCOM");
	my @modules 		= &kCom_getIniValue("kCom","MODULE");
	my $module;
	my $activeName;
	my $name;
	my $target;
	my $active_main_module;
	my $main_module;

	# print out start of page so the browser will know we have a text/html object
	print "Content-type: text/html\n\n";

	print qq(
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
		        "http://www.w3.org/TR/html4/strict.dtd">
		<html>
		<head>
			<title>$title[0]</title>
			<META HTTP-EQUIV="Pragma" CONTENT="text/html; charset=ISO-8859-1" />
			<META HTTP-EQUIV="Expires" CONTENT="0" />
			<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />
			<link rel="shortcut icon" href="$favicon[0]" />
			<link href="$css[0]" rel="stylesheet" type="text/css">
		</head>
		<body>
		<center>

		<table width="$pagewidth[0]" border="0" class="mainTable">
		<tr class="topLinks">
			<td valign="bottom" height="45">
				<a href="$url[0]" target="_top">&raquo; $title[0]</a>
			</td>
		</tr>
		<tr class="topLinks">
			<td>
				<table border="0" width="100%">
					<tr>
						<td width=\"10%\">&nbsp;</td>
						<td style="padding-right: 0px; text-align: right;">
	);
	foreach (@toplinks){
		print qq($_\n);
	}
	print qq(
						</td>
	);

	# links that only show when user is logged in
	if (&kCom_login){
		print qq(<td style="padding-right: 0px; text-align: right;">\n);
		foreach $module (@modules){

			if ($module =~ m/^(\t?)(\w+)(\t+)(\w+)(\t+)(\w+)(\t+)(.*)$/){

				$module		= $2;
				$activeName	= $4;
				$name		= $6;
				$target		= $8;

				if ($active_module eq $module){
					print qq(<a href=\"$target\">$activeName</a>\n);
				} else {
					print qq(<a href=\"$target\">$name</a>\n);
				}
			}
		}
	}

	print qq(
				</tr></table>
			</td></tr>
			<tr><td class="mainInfo">
			<!-- /kCom_printTop -->
	);

}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		Prints the bottom of every page
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_printBottom{
	my @bottomlinks		= &kCom_getIniValue("kCom","HTML_BOTTOMLINKS");
	my @statistics		= &kCom_getIniValue("kCom","HTML_STATISTICS");

	print qq(
		<!-- kCom_printBottom -->
		</td></tr>
		<tr>
			<td class="bottomInfo" colspan="2">
	);
	foreach (@bottomlinks){
		print qq($_\n);
	}
	print qq(
			</td>
		</tr>
		</table>
	);
	foreach (@statistics){
		print qq($_\n);
	}
	print qq(
		</center>
		</body>
		</html>
	);
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		starts a kCom.i style table
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_tableStart{
	my $title 		= $_[0];
	my $width 		= $_[1];

	if ($width eq undef){
		$width = "100%";
	}

	print qq(
		<table class="box" width="$width">
		<tr>
			<td class="boxHeading">
				$title
			</td>
		</tr>
		<tr>
			<td>
				<table class="boxTable">
					<tr>
						<td class="boxTable">
	);
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# desc:		ends a kCom.i style table
# - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_tableStop{
	print qq(
						</td>
					</tr>
				</table>
			</td>
		</tr>
		</table>
	);
}



sub kCom_table{
	my $title 	= $_[0];
	my $width 	= $_[1];
	my $text 	= $_[2];

	&kCom_tableStart($title, $width);
	print $text;
	&kCom_tableStop;
}



sub kCom_timestamp{
	my $option = $_[0];
	my $time = time;
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($time);
	my $timestamp;

	$year = $year+1900;
	$mon = $mon+1;

	# puts an zero in front of one digit.
	$sec =~ s/^(\d){1}$/0$1/;
	$min =~ s/^(\d){1}$/0$1/;
	$hour =~ s/^(\d){1}$/0$1/;
	$mday =~ s/^(\d){1}$/0$1/;
	$mon =~ s/^(\d){1}$/0$1/;

	if ($option == 1) {
		# 19991109
		$timestamp = $year . $mon . $mday;
	} else {
		# 19991109_182455
		$timestamp = $year . $mon . $mday . "_" . $hour . $min . $sec;
	}

	return $timestamp;

}



sub kCom_log{
	my $module = $_[0];
	my $logline = $_[1];
	my @log_dir = &kCom_getIniValue("kCom","PATH_LOG");
	my $timestamp = &kCom_timestamp();

	open (FILE, "<$log_dir[0]/$module\.log") || &kCom_error("kCom_log","failed to open '$log_dir[0]/$module\.log' for reading");
	flock (FILE, 1);		# share reading, don't allow writing
	my $content = join("",(<FILE>));
	close (FILE);
	flock (FILE, 8);		# unlock file

	# creates an entry in the log file
	open (FILE, ">$log_dir[0]/$module\.log") || &kCom_error("kCom_log","failed to open '$log_dir[0]/$module\.log' for writing");
	flock (FILE, 2);		# lock file for writing
	print FILE "$timestamp\t$module\t$logline\n" . $content;
	close (FILE);
	flock (FILE, 8);		# unlock file
}



sub kCom_addComment{
	my $comment_file		= $_[0];
	my $picture				= $_[1];
	my $return_url			= $_[2];
	my $verification_code	= &kCom_generateVerificationCode;

	&kCom_tableStart("Add comment");
	print qq(
		<form action="kComment.cgi" name="kComment" method="post">
		<input type="hidden" name="comment_file" value="$comment_file">
		<input type="hidden" name="return_url" value="$return_url">
		<br>
		<table border="0">
		<tr>
	);
	if ($picture ne undef){
		print qq(<td><img src="$picture"></td>);
	}
	print qq(
			<td valign="bottom">
			your name or email (will be displayed):<br><input type="text" name="email" size="40" maxlength="50"><br>
			your comment:<br><input type="text" name="comment" size="80" maxlength="200"><br><br>
	);
	if ($^O eq "linux"){
		print qq(
			re-type the number <img src="kImgFromText.cgi?string=$verification_code&amp;isCode=1">: <input type="text" name="vcode" size="4" maxlength="4"><br>
		);
	}
	print qq(
			</td>
		</tr>
		<tr>
			<td colspan=2 align="right">
				<a href="javascript:document.kComment.submit()">add comment</a>
			</td>
		</tr>
		</table>
		</form>

	);
	&kCom_tableStop;
}



sub kCom_listComments{
	my $file		= $_[0];
	my $style		= $_[1];

	if (-e $file){
		my $line;
		my $host;
		my $email;
		my $comment;
		my $date;

		open (FILE , "<$file");
		if ($style == 0){
			print qq(<br><br><table border="0" width="100%">);
			while ($line = <FILE>){
				($date,$host,$email,$comment) = split('���',$line);
				if ($comment ne undef){
					$comment = &kCom_parseBBCode($comment);
					$date = &kCom_printDate($date);
					print qq(
						<tr class="commentLine">
							<td width="20%">$date</td>
							<td width="20%">$host</td>
							<td>$email &nbsp;</td>
						</tr>
						<tr><td colspan="3">$comment</td></tr>
						<tr><td colspan="3">&nbsp;</td></tr>
					);
					$host=""; $email=""; $comment=""; $date = "";
				}
			}
			print qq(</table>);
		# style == 1 : used for editing purposes
		}elsif ($style == 1){
			while ($line = <FILE>){
				print $line;
			}
		}
		close (FILE);
	}
}



sub kCom_getHostIP{
	my $host_ip;
	if ($ENV{'REMOTE_HOST'}){
	        $host_ip = $ENV{'REMOTE_HOST'};
	}
	else{
	        $host_ip = $ENV{'REMOTE_ADDR'};
	}
	return $host_ip;
}



sub kCom_printDate{
	my $date = $_[0];
	my $option = $_[1];
	my $return_date;

	# 19991109_182455
	if ( $date =~ m/(\d{4})(\d{2})(\d{2})\_(\d{2})(\d{2})(\d{2})/ ){
		if ($option == 1 || $option eq undef){
			$return_date = "$1/$2/$3 - $4:$5";
		} elsif ($option == 2){
			$return_date = "$2/$3 - $4:$5";
		}
	} else { return "Unrecorgnisable dateformat" }

	return $return_date;
}



sub kCom_getLatest{
	my $module 		= $_[0];
	my $filetype	= $_[1];
	my $number		= $_[2];
	my @path;
	my $dir;
	my $file;
	my $age;
	my @filemoddate;
	my @return_files;
	my $counter = 0;

	if ($module eq "kFeeder"){
		@path = &kCom_getIniValue("kFeeder","PATH_COMMENTS");
		# have to remove the last dir, because this function searches for files in sub directories
		# of the given directory
		# ex: function serches for files in all dirs under f.ex "../kForum/",
		# that means that "../kForum/threads" is searched
		$path[0] =~ s/\/\w+$//;
	} else {
		return;
	}

	# gets all the directories
	opendir (D, "$path[0]");
	my @directories = grep/\w/, readdir(D);
	closedir(D);

	foreach $dir (@directories){
		# check the date for all files in the directory
		opendir (D, "$path[0]/$dir");
		my @files = grep/$filetype$/, readdir(D);
		closedir(D);

		foreach $file (@files){
			push(@filemoddate, &kCom_getAgeOfFileString("$path[0]/$dir/$file"));
		}
	}

	# makes a sorted array out of files
	foreach (sort @filemoddate) {
		if ($counter < $number){
			$_ =~ s/^.*���//;
			push (@return_files, $_);
			$counter++;
	    }
	}

	return @return_files;
}



sub kCom_kFeeder_getLatestComments{

	my $number 	= $_[0];
	my @latest;
	my @comment_dir = &kCom_getIniValue("kFeeder","PATH_COMMENTS");
	my $line;
	my @url;
	my $fileAge;

	&kCom_tableStart("$number recent commented feeds");

	@latest = &kCom_getLatest("kFeeder",".com",$number);
	print "<table>\n";
	foreach (@latest){
		$fileAge = (-M $comment_dir[0]."/".$_);
		$fileAge =~ s/(^.{5}).*/$1/;
		@url = &kCom_getIniValue($comment_dir[0]."/".$_,"RETURN_URL");
		$url[0] =~ s/&/&amp;/sgi;
		print qq(<tr><td><a href="$url[0]">$_</a> ($fileAge)</td></tr>\n);
	}

	print "</table>\n";
	&kCom_tableStop;
}



sub kCom_kIndexPlugin_news{

	my $number		= $_[0];
	my $line;
	my @newsfile	= &kCom_getIniValue("kNews","PATH_NEWSFILE");
	my $counter		= 1;

	my $timestamp;
	my $host_ip;
	my $message;

	&kCom_tableStart("News","100%");
	print "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";

    open(FILE, "<$newsfile[0]");
	LINE: while ($line = <FILE>){

		if ($line =~ m/^(\d{8}\_\d{6})���(.*)���(.*)$/){
			($timestamp,$host_ip,$message) = ($1,$2,$3);
			$timestamp = &kCom_printDate($timestamp, 2);
			print "<tr><td>$timestamp: '$message'</td></tr>\n";
			if ($counter < $number){
				$counter++;
			} else {
				last LINE;
			}
		}
	}
	close (FILE);

	print "</table>\n";
	&kCom_tableStop;

}



sub kCom_kIndexPlugin_contactInfo{

	my @address 	= &kCom_getIniValue("kIndexPlugin","CONTACT_INFO");

	&kCom_tableStart("Contact Info","");

	foreach (@address){
		print "$_\n";
	}

	&kCom_tableStop;

}



sub kCom_getMostPop{

	my $type		= $_[0];
	my $number		= $_[1];
	my $line;
	my @access_log 	= &kCom_getIniValue("kCom","PATH_APACHE_ACCESS_LOG");
	my $counter		= 0;
	my $title;
	my $entry;
	my %results;
	my $max_hits = 0;
	my @res;
	my $i = 0;

	# 127.0.0.1 - - [27/May/2007:18:31:50 +0200] "GET /0705_kComIntegrated/kCom.i/cgi/kFeeder.cgi?p_action=show&p_feeder=kFeeder_klevstulBlogspot&p_url=2456331563515016209 HTTP/1.1" 200 4888

	if ($type eq "kFeeder"){
		$title = "Popular feed objects";
	}

    open(FILE, "<$access_log[0]") || &kCom_error("kCom_getMostPop","failed to open '$access_log[0]'");;
	LINE: while ($line = <FILE>){
		$line =~ s/(.*)HTTP\/1\..*/$1/g;															# remove everything behind HTTP, otherwise it'll be wrong when refering site is logged in same logfile

		if ($type eq "kFeeder"){
			if ($line =~ m/GET.*(kFeeder\.cgi.*)/){
				$entry = $1;
			}

			if ($entry ne ""){
				$results{$entry}++;
				$entry = "";
			}
		}
	}
	close (FILE);

	# create a sorted array based on the entries
	foreach (keys %results){
		push(@res, kCom_getSortableNumberString($results{$_}) . "���" . $_);
	}

	&kCom_tableStart($title);
	print "<table cellpadding=\"1\" cellspacing=\"0\">\n";

	if ($type eq "kFeeder"){
		FOREACH: foreach (reverse sort @res){
			if ($i < $number){
				($counter,$entry) = split("���",$_);
				if ($counter =~ m/^0*(.*)/ ){
					$counter = $1;
				}
				$entry =~ s/&/&amp;/sgi;
				print qq(
					<tr><td style="text-align: right;">$counter:</td><td><a href="$entry">$entry</a></td></tr>
				);
				$i++;
			} else {
				last FOREACH;
			}
		}
	}

	print "</table>\n";
	&kCom_tableStop;
}



sub kCom_kFeeder_getMostPop{
	my $number		= $_[0];
	&kCom_getMostPop("kFeeder", $number);
}



sub kCom_getAgeOfFileString{
	my $file 		= $_[0];
	my $age = (-M "$file");
	my $return_string;
	my $filename	= $file;
	$filename =~ s/^.*\///;
	$return_string = kCom_getSortableNumberString($age) . "���" . $filename;
}



sub kCom_getSortableNumberString{
	my $number 		= $_[0];
	if ($number<=9){
		$return_string =  "00000" . $number;
	} elsif ($number<=99) {
		$return_string =  "0000" . $number;
	} elsif ($number<=999) {
		$return_string =  "000" . $number;
	} elsif ($number<=9999) {
		$return_string =  "00" . $number;
	} elsif ($number<=99999) {
		$return_string =  "0" . $number;
	}
	return $return_string;
}



sub kCom_jumpTo{
	my $url			= $_[0];
	my $ctype		= $_[1];
	my $wait		= $_[2];

	my @css			= &kCom_getIniValue("kCom","HTML_CASCADINGSTYLESHEET");

	if ($ctype == 1){
		print "Content-type: text/html\n\n";
	}

	if ($wait eq undef){
		$wait = 0;
	}

	print qq(<html><head><title> ::: kCom.i ::: integration solution ::: </title>
		<META HTTP-EQUIV="REFRESH" Content="$wait;URL=$url">
		<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
		<link href="$css[0]" rel="stylesheet" type="text/css">
		</head><body >Please wait...</body></html>
	);
}



sub kCom_parseBBCode{
	my $string = $_[0];

	my $reg_email		= '[\w|\d|\.|\-]*\@{1}[\w|\d|\.|\-]*';
	my $reg_linkname 	= '[\w|\d|\.|\-|\s|&|\@|=|!|\/|\'|`|)|(|:|\?|<|>|"]*';
	my $reg_url			= '[\w|\d|\.|\/|\?|\$|:|\-|~|\@|&|=|\+|\%|;|#|,|)|(]*';
	my $reg_color		= '[\w|#|\d]*';
	my @img				= &kCom_getIniValue("kCom","URL_IMG");

	# -------------
	# [quote][/quote]
	# -------------
	$string =~ s/\[quote="?($reg_linkname)"?\]/$1: <i>"/ig;
	$string =~ s/\[quote\]/<i>"/ig;
	$string =~ s/\[\/quote\]/"<\/i>/ig;

	# ------------
	# [b][/b]
	# ------------
	$string =~ s/\[b\]/<b>/ig;
	$string =~ s/\[\/b\]/<\/b>/ig;

	# ------------
	# [i][/i]
	# ------------
	$string =~ s/\[i\]/<i>/ig;
	$string =~ s/\[\/i\]/<\/i>/ig;

	# ------------
	# [u][/u]
	# ------------
	$string =~ s/\[u\]/<u>/ig;
	$string =~ s/\[\/u\]/<\/u>/ig;

	# ------------
	# [color][/color]
	# ------------
	$string =~ s/\[colou?r="?($reg_color)"?\]/<font color="$1">/ig;
	$string =~ s/\[\/colou?r\]/<\/font>/ig;

	# ------------
	# [size][/size]
	# ------------
	$string =~ s/\[size="?(\d+)"?\]/<font size="$1">/ig;
	$string =~ s/\[\/size\]/<\/font>/ig;

	# ------------
	# [img][/img]
	# ------------
	$string =~ s/\[img\]($reg_url)\[\/img\]/<img src="$1">/ig;
	$string =~ s/\[img="?($reg_url)"?\]($reg_linkname)\[\/img\]/<img src="$1" title="$2">/ig;

	# ------------
	# [pre][/pre]
	# ------------
	$string =~ s/\[pre\]/<pre>/ig;
	$string =~ s/\[\/pre\]/<\/pre>/ig;

	# -------------
	# [dice][/dice]
	# -------------
	$string =~ s/\[dice\]\s?(\d{1})\s?\[\/dice\]/<img src="$img[0]\/dice$1\.gif">/ig;

	# -------------
	# [email][/email]
	# -------------
	$string =~ s/\[email/\[url/ig;
	$string =~ s/\[\/email\]/\[\/url\]/ig;

	# -------------
	# [url][/url]
	# -------------
	# [url]mailto:frode@klevstul.com[/url]
	$string =~ s/\[url\](mailto:)($reg_email)\[\/url\]/<a href="$1$2">$2<\/a>/ig;
	# [url]frode@klevstul.com[/url]
	$string =~ s/\[url\]($reg_email)\[\/url\]/<a href="mailto:$1">$1<\/a>/ig;
	# [url=mailto:frode@klevstul.com]send email[/url]
	$string =~ s/\[url="?(mailto:)($reg_email)"?\]($reg_linkname)\[\/url\]/<a href="$1$2">$3<\/a>/ig;
	# [url=frode@klevstul.com]send email[/url]
	$string =~ s/\[url="?($reg_email)"?\]($reg_linkname)\[\/url\]/<a href="mailto:$1">$2<\/a>/ig;

	# [url]http://klevstul.com[/url]
	$string =~ s/\[url\](https?|ftp)(:\/\/)($reg_url)\[\/url\]/<a href="$1$2$3" target="_blank">$3<\/a>/ig;
	# [url]klevstul.com[/url]
	$string =~ s/\[url\]($reg_url)\[\/url\]/<a href="http:\/\/$1" target="_blank">$1<\/a>/ig;
	# [url=http://klevstul.com]visit this site[/url]
	$string =~ s/\[url="?(https?|ftp)($reg_url)"?\]($reg_linkname)\[\/url\]/<a href="$1$2" target="_blank">$3<\/a>/ig;
	# [url=klevstul.com]visit this site[/url]
	$string =~ s/\[url="?($reg_url)"?\]($reg_linkname)\[\/url\]/<a href="http:\/\/$1" target="_blank">$2<\/a>/ig;

	return $string;
}



sub kCom_login{

	my @cookie_name = &kCom_getIniValue("kLogin","COOKIE");
	my $cookie		= &kCom_getCookie($cookie_name[0]);
	my $login;

	if ($cookie eq "loggedIn"){
		$login = 1;
	} else {
		$login = 0;
	}

	return $login;
}



sub kCom_notLoggedInMessage{

	&kCom_printError("kLogin","Please <b>LOGIN</b> to access this page!<br><br>In case you've already logged in, please <b>RELOAD</b> this page to view it!");

}



sub kCom_setCookie {
	# end a set-cookie header with the word secure and the cookie will only
	# be sent through secure connections

	my ($name, $value, $expires, $path, $domain, $secure) = @_;

	$name 			= &kCom_cookieScrub($name);
	$value 			= &kCom_cookieScrub($value);

	$expires		= $expires * 60;

	my $expire_at 	= &kCom_cookieDate($expires);
	my $namevalue 	= "$name=$value";

	my $COOKIE		= "";

	if ($expires != 0) {
		$COOKIE		= "Set-Cookie: $namevalue; expires=$expire_at; ";
	}
	else {
		$COOKIE		= "Set-Cookie: $namevalue; ";   #current session cookie if 0
	}
	if ($path ne ""){
		$COOKIE 	.= "path=$path; ";
	}
	if ($domain ne ""){
		$COOKIE 	.= "domain=$domain; ";
	}
	if ($domain ne ""){
		$COOKIE 	.= "domain=$domain; ";
	}
	if ($secure ne ""){
		$COOKIE 	.= "$secure ";
	}

	print $COOKIE . "\n";
}



sub kCom_cookieScrub {
	# don't allow = or ; as valid elements of name or data

	my($retval) = @_;

	$retval=~s/\;//;
	$retval=~s/\=//;

	return $retval;
}



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# this routine accepts the number of seconds to add to the server
# time to calculate the expiration string for the cookie. Cookie
# time is ALWAYS GMT!
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
sub kCom_cookieDate {

  my ($seconds) = @_;

  my %mn = ('Jan','01', 'Feb','02', 'Mar','03', 'Apr','04',
            'May','05', 'Jun','06', 'Jul','07', 'Aug','08',
            'Sep','09', 'Oct','10', 'Nov','11', 'Dec','12' );
  my $sydate=gmtime(time+$seconds);
  my ($day, $month, $num, $time, $year) = split(/\s+/,$sydate);
  my    $zl=length($num);
  if ($zl == 1) {
    $num = "0$num";
  }

  my $retdate="$day $num-$month-$year $time GMT";

  return $retdate;
}



sub kCom_getCookie {

	my $name = $_[0];
	my $temp=$ENV{'HTTP_COOKIE'};
	my $key;
	my $value;

	# cookies are seperated by a semicolon and a space, this will split
	# them and return a hash of cookies
	@pairs=split(/\; /,$temp);

	foreach my $sets (@pairs) {
		($key,$value)=split(/=/,$sets);
		$clist{$key} = $value;
	}

  	my $retval=$clist{$name};

	return $retval;

}



sub kCom_verifyCode {
	my $code		= $_[0];
	my @verify_dir	= &kCom_getIniValue("kCom","PATH_VERIFY");
	my $file		= "$verify_dir[0]/$code.ver";
	my $time		= time();
	my $f;
	my $return_value;

	# return true if file exists on OS
	if (-e $file){
		unlink $file;
		$return_value = 1;
	} else {
		$return_value = 0;
	}

	# cleanup old files (http://www.infocopter.com/perl/cleanup.html)
	opendir(D, $verify_dir[0]) || &kCom_error("kCom_verifyCode","failed to open '$verify_dir[0]");
	while ($f = readdir(D)) {
		next if $f =~ /^\./;
		next if $f !~ /^\d*\.ver/;																# has to be on the form "[digit].ver"

		my ($atime, $mtime, $ctime) = (stat($verify_dir[0]."/".$f))[8..10];
		my $age_hours = ($time - $mtime) / 3600;
		my $age_days  = int($age_hours / 24);

		# 0.1 equals six minutes, 1.0 = 1 hour (of course)
		next unless $age_hours > 1;

		unlink $verify_dir[0]."/".$f;
	}
	closedir(D);

	return $return_value;
}



sub kCom_generateVerificationCode
{
	my @verify_dir		= &kCom_getIniValue("kCom","PATH_VERIFY");
	my $random_number	= int(rand(9999));
	my $timestamp		= &kCom_timestamp();

	# create an file in the verification directory having the same name as the generated number
	open (FILE, ">$verify_dir[0]/$random_number.ver") || &kCom_error("kCom_generateVerificationCode","failed to open '$verify_dir[0]/$random_number.ver' for writing");
	flock (FILE, 2);		# lock file for writing
	print FILE "$timestamp\n";
	close (FILE);
	flock (FILE, 8);		# unlock file

	# always change this number for security, it will be fixed again when printed out as gif
	$random_number = $random_number * 180876;

	return $random_number;
}
