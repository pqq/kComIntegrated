#!<�-PERL_PATH-�>

# ---------------------------------------------
# filename: kSearch.cgi
# author:   Frode Klevstul (frode@klevstul.com)
# started:  23.12.2007
# build:  	<�-BUILD_VERSION-�>
# version:	v01_20071223
# ---------------------------------------------



# ----------------------------------
# use
# ----------------------------------
use kCom;



# ------------------
# declarations
# ------------------


# ------------------
# main
# ------------------
&kCom_printTop("kSearch");
&kSearch_main;
&kCom_printBottom;



# ------------------
# sub
# ------------------
sub kSearch_main {
    my @pathGoogle = &kCom_getIniValue("kSearch","PATH_GOOGLE");
    my @searchSite = &kCom_getIniValue("kSearch","SEARCH_SITE");

	kCom_tableStart("Search");
	print qq(
		<table width="100%">
		<tr><td>
		    <script src="$pathGoogle[0]" type="text/javascript"></script>
		    <script language="Javascript" type="text/javascript">
	
		    //<![CDATA[
	
		    google.load("search", "1");
	
			function OnLoad() {
				var searchControl = new google.search.SearchControl();
	
		        options = new GsearcherOptions();
		        options.setExpandMode(GSearchControl.EXPAND_MODE_OPEN);
	
				var siteSearch = new GwebSearch();
				siteSearch.setUserDefinedLabel("$searchSite[0]");
				siteSearch.setUserDefinedClassSuffix("siteSearch");
				siteSearch.setSiteRestriction("$searchSite[0]");
	
				siteSearch.setResultSetSize(GSearch.LARGE_RESULTSET);
				searchControl.addSearcher(siteSearch, options);
	
				searchControl.draw(document.getElementById("searchcontrol"));
		    }
	
		    google.setOnLoadCallback(OnLoad);
	
		    //]]>
		    </script>

		</td></tr>
		<tr><td class="googleSearch" id="searchcontrol">
			loading...
		</td></tr>
		</table>
	);
	kCom_tableStop;

}
